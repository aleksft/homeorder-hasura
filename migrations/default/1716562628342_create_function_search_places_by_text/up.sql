CREATE
OR REPLACE FUNCTION public.search_places_by_text(searchName text)
RETURNS SETOF places
LANGUAGE sql
STABLE AS
$function$
SELECT
  *
FROM
  places
WHERE
  unaccent(name) ilike '%' || unaccent(searchName) || '%';
$function$;
