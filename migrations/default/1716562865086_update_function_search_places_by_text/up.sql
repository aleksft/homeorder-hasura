DROP FUNCTION public.search_places_by_text;

CREATE OR REPLACE FUNCTION public.search_places_by_text(search_name text)
 RETURNS SETOF places
 LANGUAGE sql
 STABLE
AS $function$
SELECT
  *
FROM
  places
WHERE
  unaccent(name) ilike '%' || unaccent(search_name) || '%';
$function$;
