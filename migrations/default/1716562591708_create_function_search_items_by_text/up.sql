CREATE
OR REPLACE FUNCTION public.search_items_by_text(searchName text, searchNotes text)
RETURNS SETOF items
LANGUAGE sql
STABLE AS
$function$
SELECT
  *
FROM
  items
WHERE
  unaccent(name) ilike '%' || unaccent(searchName) || '%' AND
  unaccent(notes) ilike '%' || unaccent(searchNotes) || '%';
$function$;
