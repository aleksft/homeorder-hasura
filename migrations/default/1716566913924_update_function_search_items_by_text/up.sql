CREATE OR REPLACE FUNCTION public.search_items_by_text(search_name text, search_notes text)
 RETURNS SETOF items
 LANGUAGE sql
 STABLE
AS $function$
SELECT
  *
FROM
  items
WHERE
  (length(search_name) = 0 OR unaccent(name) ilike '%' || unaccent(search_name) || '%') AND
  (length(search_notes) = 0 OR unaccent(notes) ilike '%' || unaccent(search_notes) || '%');
$function$;
