alter table "public"."items"
  add constraint "items_type_id_fkey"
  foreign key ("type_id")
  references "public"."item_types"
  ("value") on update restrict on delete restrict;
