CREATE OR REPLACE FUNCTION public.search_items_by_name(search text)
RETURNS SETOF items
LANGUAGE sql
STABLE
AS
$function$
    SELECT *
    FROM items
    WHERE unaccent(name) ilike '%' || unaccent(search) || '%';
$function$;
