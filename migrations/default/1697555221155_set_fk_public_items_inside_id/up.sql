alter table "public"."items"
  add constraint "items_inside_id_fkey"
  foreign key ("inside_id")
  references "public"."items"
  ("id") on update restrict on delete restrict;
