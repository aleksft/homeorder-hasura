alter table "public"."items"
  add constraint "items_place_id_fkey"
  foreign key ("place_id")
  references "public"."places"
  ("id") on update restrict on delete restrict;
