DROP FUNCTION public.search_places_by_text;

CREATE OR REPLACE FUNCTION public.search_places(search_name text)
 RETURNS SETOF places
 LANGUAGE sql
 STABLE
AS $function$
SELECT
  *
FROM
  places
WHERE
  (length(search_name) = 0 OR unaccent(name) ilike '%' || unaccent(search_name) || '%');
$function$;
