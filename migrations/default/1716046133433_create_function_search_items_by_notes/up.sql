CREATE OR REPLACE FUNCTION public.search_items_by_notes(search text)
RETURNS SETOF items
LANGUAGE sql
STABLE
AS
$function$
    SELECT *
    FROM items
    WHERE unaccent(notes) ilike '%' || unaccent(search) || '%';
$function$;
