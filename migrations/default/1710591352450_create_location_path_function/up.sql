create or replace function public.location_path(_itemid int)
  returns text
  language sql
  stable
as $function$
  with recursive item_location_path as (
    select id, 1 as rowno, name from items
    union
    select i.id, rowno +1 as rowno, ilp.name 
    from items i inner join item_location_path ilp
    on ilp.id = i.inside_id
  )
  SELECT STRING_AGG(name, ' > ') FROM (
    select * from item_location_path where rowno > 1 and id = _itemid order by rowno desc
  ) AS aux;
$function$
