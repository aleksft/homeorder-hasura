DROP FUNCTION public.search_place_by_name;

CREATE OR REPLACE FUNCTION public.search_places_by_name(search text)
RETURNS SETOF places
LANGUAGE sql
STABLE
AS
$function$
    SELECT *
    FROM places
    WHERE unaccent(name) ilike '%' || unaccent(search) || '%';
$function$;
