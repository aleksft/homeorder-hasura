DROP FUNCTION public.search_items_by_text;

CREATE OR REPLACE FUNCTION public.search_items_by_text(search_name text, search_notes text)
 RETURNS SETOF items
 LANGUAGE sql
 STABLE
AS $function$
SELECT
  *
FROM
  items
WHERE
  unaccent(name) ilike '%' || unaccent(search_name) || '%' AND
  unaccent(notes) ilike '%' || unaccent(search_notes) || '%';
$function$;
